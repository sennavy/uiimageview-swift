//
//  ImageSourceOptionVC.swift
//  BNKCL
//
//  Created by Navy on 30/8/22.
//  Copyright © 2022 KOSIGN. All rights reserved.


import UIKit

class ImageSourceOptionVC: UIViewController {
    
    // MARK: - IBOutlet -
    @IBOutlet weak var dismissButton    : UIButton!
    @IBOutlet weak var tableView        : UITableView!
    @IBOutlet weak var tableViewBottom  : NSLayoutConstraint!
    @IBOutlet weak var tableViewHeight  : NSLayoutConstraint!
    
    
    // MARK: - Variable
    var draftDocumentCompletion     = { }
    var externalDocumentCompletion  = { }
    var attachmentCompletion        = { }
    
    var arrayMenu                   = [String]()
    var arrayIconImage              = [String]()
    
    // MARK: - IBAction
    @IBAction func dismissButtonClicked(_ sender: UIButton) {
        self.back()
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayMenu = ["Choose from allary", "Take a photo"]
        arrayIconImage = ["gallery_ico_sheet","camera_ico_sheet"]
   
        setupConstraint: do {
            // Height for Row + Number of Rows + View Height
            let countMenu   = CGFloat(arrayMenu.count)
            let height      = (50 * countMenu) + 20
            tableViewHeight.constant = height
            tableViewBottom.constant = -height
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2) {
                self.tableViewBottom.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension ImageSourceOptionVC {
    func back(completion: @escaping Completion = { }) {
        UIView.animate(withDuration: 0.2, animations: {
            let countMenu                   = CGFloat(self.arrayMenu.count)
            self.tableViewBottom.constant   = -(50 * countMenu + 56) // -(206 + 34)
            self.view.layoutIfNeeded()
        }) { _ in
            self.dismiss(animated: true)
            completion()
        }
    }
}

extension ImageSourceOptionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageSourceOptionCell") as! ImageSourceOptionCell
        
        cell.iconImage.image = UIImage(named: arrayIconImage[indexPath.row])
        cell.titleLabel.text = arrayMenu[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath) as! ImageSourceOptionCell
        selectedCell.contentView.backgroundColor = .white
        switch indexPath.row {
        case 0:
            self.back {
                self.attachmentCompletion()
            }
        case 1:
            self.back {
                self.draftDocumentCompletion()
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath) as! ImageSourceOptionCell
        selectedCell.contentView.backgroundColor    = .white
        selectedCell.titleLabel.textColor           = .red
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath) as! ImageSourceOptionCell
        selectedCell.contentView.backgroundColor    = .white
        selectedCell.titleLabel.textColor           = .gray
    }
}
