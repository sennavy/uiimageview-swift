//
//  ImageSourceOptionCell.swift
//  Bizcard4.0
//
//  Created by Navy on 30/8/22.
//  Copyright © 2022 KOSIGN. All rights reserved.


import UIKit

class ImageSourceOptionCell : UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var titleLabel   : UILabel!
    @IBOutlet weak var iconImage    : UIImageView!
    
    
    // MARK: - Variable
    override open var frame : CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame           =  newFrame
            frame.origin.y      += 20
            frame.origin.x      += 0
            frame.size.height   -= 10
            frame.size.width    -= 10
            super.frame         = frame
        }
    }
}
