//
//  MainMenuVM.swift
//  UIImageView
//
//  Created by Navy on 7/10/22.
//

import Foundation

enum RowName {
    case SekeletonView
    case AccessToGalleryViaLibrary
    case AccessToGalleryPickerController
    case ImageCompression
    case DownloadImage
}

class MainMenuVM {
    
    var cells    :[MainMenuModel<Any>] = []
    
    func initCell() {
        cells = []
        cells.append(MainMenuModel<Any>(value: (MainMenuInfo(title: "1. SkeletonView", rowName: .SekeletonView))))
        cells.append(MainMenuModel<Any>(value: (MainMenuInfo(title: "2. Accessing to Gallery & Camera (Library)", rowName: .AccessToGalleryViaLibrary))))
        cells.append(MainMenuModel<Any>(value: (MainMenuInfo(title: "3. Accessing to Gallery & Camera (Native)", rowName: .AccessToGalleryPickerController))))
        cells.append(MainMenuModel<Any>(value: (MainMenuInfo(title: "4. Image Compression", rowName: .ImageCompression))))
        cells.append(MainMenuModel<Any>(value: (MainMenuInfo(title: "5. Download Image by URL String", rowName: .DownloadImage))))
    }
}
