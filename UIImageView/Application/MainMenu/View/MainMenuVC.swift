//
//  MainMenuVC.swift
//  UIImageView
//
//  Created by Navy on 7/10/22.
//

import UIKit

class MainMenuVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let mainMenuVM = MainMenuVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainMenuVM.initCell()
    }
}

extension MainMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        mainMenuVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = mainMenuVM.cells[indexPath.row].value as! MainMenuInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainMenuCell", for: indexPath) as! MainMenuCell
        cell.configCell(title: data.title)
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = mainMenuVM.cells[indexPath.row].value as! MainMenuInfo
        switch data.rowName {
        case .SekeletonView:
            let vc = UIStoryboard.init(name: "SkeletonViewSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "SkeletonViewVC") as? SkeletonViewVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .AccessToGalleryViaLibrary:
            let vc = UIStoryboard.init(name: "ImagePickerNativeSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "ImagePickerNativeVC") as? ImagePickerNativeVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .AccessToGalleryPickerController:
            let vc = UIStoryboard.init(name: "ImagePickerLibrarySB", bundle: Bundle.main).instantiateViewController(withIdentifier: "ImagePickerLibraryVC") as? ImagePickerLibraryVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .ImageCompression:
            let vc = UIStoryboard.init(name: "ImageCompressionSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "ImageCompressionVC") as? ImageCompressionVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .DownloadImage:
            let vc = UIStoryboard.init(name: "DownloadImageSB", bundle: Bundle.main).instantiateViewController(withIdentifier: "DownloadImageVC") as? DownloadImageVC
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        
    }
    
}
