//
//  ImagePickerVC.swift
//  UIImageView
//
//  Created by Navy on 7/10/22.
//

import UIKit

enum ImageType {
    case Cover
    case Profile
}

class ImagePickerNativeVC: UIViewController {
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var profileImageView    : UIImageView!
    @IBOutlet weak var coverImageView      : UIImageView!
    
    
    // MARK: - Properties
    var imageType       = ImageType.Cover
    var profileImage    : UIImage?
    var coverImage      : UIImage?

    // MARK: - LifeCle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }

    
    
    // MARK: - Functions

    func showImagePickerOptions() {
        let alertVC = UIAlertController(title: "Choose a photo", message: "Choose a picture from Library or Camera", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] (action) in
            guard let self = self else {
                return
            }
            
            let cameraImagePicker = self.imagePicker(sourceType: .camera)
            cameraImagePicker.delegate = self
            cameraImagePicker.modalPresentationStyle = .fullScreen
            self.present(cameraImagePicker, animated: true)
        }
        
        
        let libraryAction = UIAlertAction(title: "Gallery", style: .default) {[weak self] (action) in
            
            guard let self = self else {
                return
            }
            
            let libraryImagePicker = self.imagePicker(sourceType: .photoLibrary)
            libraryImagePicker.modalPresentationStyle = .fullScreen
            libraryImagePicker.delegate = self
            self.present(libraryImagePicker, animated: true) {
            
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addAction(cameraAction)
        alertVC.addAction(libraryAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    
    func imagePicker(sourceType: UIImagePickerController.SourceType) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        return imagePicker
    }
    
    
    func setupView() {
        if coverImage == nil {
            coverImageView.image = UIImage(named: "empty_cover")
        } else {
            coverImageView.image = coverImage
        }
        
        
        if profileImage == nil {
            profileImageView.image = UIImage(named: "empty_profile")
        } else {
            profileImageView.image = profileImage
        }
    }
    
    // MARK: - IBAction
    @IBAction func pickProfilePictureButtonDidTap(_ sender: UIButton) {
        imageType = .Profile
        showImagePickerOptions()
    }
    
    @IBAction func pickCoverImageButtonDidTap(_ sender: UIButton) {
        imageType = .Cover
        showImagePickerOptions()
    }

}

// Completion choose image from Gallery or Camera
extension ImagePickerNativeVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        let img = info[.originalImage] as! UIImage
        
        switch imageType {
        case .Cover:
            coverImage = img
            coverImageView.image = coverImage
            
        case .Profile:
            profileImage = img
            profileImageView.image = profileImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}






// REFERENCE : https://medium.com/nerd-for-tech/how-to-display-an-image-picker-controller-using-swift-5cfa9892d0b6


