//
//  ImageCompressionVC.swift
//  UIImageView
//
//  Created by Navy on 11/10/22.
//

import UIKit
import iOSDropDown

enum JPEGQuality: CGFloat {
    case lowest  = 0
    case low     = 0.25
    case medium  = 0.5
    case high    = 0.75
    case highest = 1
}

class ImageCompressionVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var coverImageView               : UIImageView!
    @IBOutlet weak var resultImageView              : UIImageView!
    @IBOutlet weak var dropDown                     : DropDown!
    @IBOutlet weak var progressView                 : UIView!
    @IBOutlet weak var progressViewWidthConstraint  : NSLayoutConstraint!
    @IBOutlet weak var progressLabel                : UILabel!
    @IBOutlet weak var progressViewHeightContraint  : NSLayoutConstraint!
    @IBOutlet weak var progressContainerView        : UIView!
    
    @IBOutlet weak var compressButton               : UIButton!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    // MARK: - Variable
    var originalImage       : UIImage?
    var compressedImage     : UIImage?
    var progressValue       = 0.0
    var compressionLevel    = JPEGQuality.medium
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViews()
        self.initDropdown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.largeTitleDisplayMode = .never
    }
    
    // MARK: - Functions
    func initViews() {
        self.progressViewHeightContraint.constant       = 0
        self.progressViewWidthConstraint.constant       = 0
        self.progressLabel.text                         = "Compressing..."
        self.progressView.backgroundColor               = .systemGray2
        self.progressContainerView.borderColor          = .systemGray
        self.progressLabel.isHidden                     = true
        self.compressButton.backgroundColor             = .systemGray2
        self.compressButton.isUserInteractionEnabled    = false
        self.resultImageView.isHidden                   = true
        self.resultLabel.isHidden                       = true
    }
    
    func initDropdown(){
        dropDown.text = "Medium"
        // The list of array to display. Can be changed dynamically
        dropDown.optionArray = ["Lowest", "Low", "Medium", "High", "Highest"]
        //Its Id Values and its optional
        dropDown.optionIds = [0, 25, 50, 75, 100]
        // Image Array its optional
        // The the Closure returns Selected Index and String
        dropDown.didSelect{(selectedText , index ,id) in
            
            self.changeCompresLevel()
            
            switch id {
            case 0  : self.compressionLevel = .lowest
            case 25 : self.compressionLevel = .low
            case 50 : self.compressionLevel = .medium
            case 75 : self.compressionLevel = .high
            default : self.compressionLevel = .highest
            }
            self.dropDown.text = "\(selectedText)"
        }
    }
    
    func showImagePickerOptions() {
        let alertVC = UIAlertController(title: "Choose a photo", message: "Choose a picture from Library or Camera", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] (action) in
            guard let self = self else {
                return
            }
            
            let cameraImagePicker = self.imagePicker(sourceType: .camera)
            cameraImagePicker.delegate = self
            cameraImagePicker.modalPresentationStyle = .fullScreen
            self.present(cameraImagePicker, animated: true)
        }
        
        
        let libraryAction = UIAlertAction(title: "Gallery", style: .default) {[weak self] (action) in
            
            guard let self = self else {
                return
            }
            
            let libraryImagePicker = self.imagePicker(sourceType: .photoLibrary)
            libraryImagePicker.modalPresentationStyle = .fullScreen
            libraryImagePicker.delegate = self
            self.present(libraryImagePicker, animated: true) {
            
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addAction(cameraAction)
        alertVC.addAction(libraryAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    func imagePicker(sourceType: UIImagePickerController.SourceType) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        return imagePicker
    }
    
    func resetView() {
        self.progressViewHeightContraint.constant        = 25
        self.progressViewWidthConstraint.constant        = 0
        self.progressLabel.text                          = "Compressing..."
        
        self.progressLabel.textColor                     = .systemGray
        self.progressView.backgroundColor                = .systemGray2
        self.progressContainerView.borderColor           = .systemGray
        self.progressLabel.isHidden                      = false
        self.compressButton.backgroundColor              = .systemGreen
        self.compressButton.isUserInteractionEnabled     = true
        self.resultImageView.image                       = UIImage(named: "placeholder")
    }
    
    func changeCompresLevel() {
        self.progressViewHeightContraint.constant        = 0
        self.progressViewWidthConstraint.constant        = 0
        self.progressLabel.text                          = "Compressing..."
        
        self.progressLabel.textColor                     = .systemGray
        self.progressView.backgroundColor                = .systemGray2
        self.progressContainerView.borderColor           = .systemGray
        self.progressLabel.isHidden                      = true
        self.compressButton.backgroundColor              = .systemGreen
        self.compressButton.isUserInteractionEnabled     = true
        self.resultImageView.image                       = UIImage(named: "placeholder")
    }
    
    func clearResult(){
        self.progressViewHeightContraint.constant        = 0
        self.resultImageView.image  = UIImage(named: "placeholder")
        self.resultImageView.isHidden = false
        self.resultLabel.isHidden = false
    }
    
    func compressCompletion(){
        self.resultLabel.isHidden = false
        self.resultImageView.isHidden = false
        self.progressContainerView.borderColor = .systemGreen
        self.progressLabel.text             = "Done"
        self.progressView.backgroundColor   = .systemGreen
        self.progressLabel.textColor        = .white
        self.resultImageView.image          = self.compressedImage
    }
    
    func compressImage() {
        
        self.resetView()
    
        if let imageData = originalImage?.compressImage(compressionLevel) {
            
            // compression result
            print("Original image size : \(imageData.count)")
            self.compressedImage = UIImage(data: imageData)
            
            // update progress view
            self.progressLabel.isHidden = false
            self.progressViewHeightContraint.constant = 25
            
            // progress animation
            view.layoutIfNeeded()
            self.progressViewWidthConstraint.constant = screenWidth - 50
            UIView.animate(withDuration: 3, animations: {
                self.view.layoutIfNeeded()
                
                // Compress completion
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.1) {
                    self.compressCompletion()
                }
            })
        }
    }
    
    
    // MARK: - @IBAction
    @IBAction func chooseImageButtonDidTap(_ sender: UIButton) {
        self.showImagePickerOptions()
    }
    
    
    @IBAction func compressButtonDidTap(_ sender: Any) {
        self.compressImage()
    }
    
    
    @IBAction func backBarButtonItem(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}


// MARK: - UIImagePickerController
extension ImageCompressionVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let img = info[.originalImage] as! UIImage
        
        let imgData = img.pngData()
        print("Original image size : \(imgData?.count ?? 0)")
        
        if originalImage != img && originalImage != nil {
            self.clearResult()
            self.originalImage = img
        } else {
            self.originalImage = img
        }
        
        self.coverImageView.image = self.originalImage

        // enable button
        compressButton.backgroundColor = .systemGreen
        compressButton.isUserInteractionEnabled = true
        
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UIImage
extension UIImage {
    func compressImage(_ quality: JPEGQuality) -> Data? {
        return self.jpegData(compressionQuality: quality.rawValue)
    }
}

